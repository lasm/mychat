// https://blog.risingstack.com/your-first-node-js-http-server/
// https://websockets.github.io/ws/
// http://stackoverflow.com/questions/6084360/using-node-js-as-a-simple-web-server
// https://www.npmjs.com/package/ws
// https://github.com/websockets/ws

var testvar1 = 'testvar1_value';
Tools = require('./debugtool.js');
var bd = Tools();
var bb1 = {dd: 33, kk: {data: 'sdn'}};
bd(bb1);

function store() {
	this.a = [];
	this.add = function(key, value) {
		}; // add
}; // store

var d = 1 + new Date();
bd(d);
//console.log('cols:', util.inspect(process.stdout));

var http = require('http');
var fs = require('fs');
var htmlfile = fs.readFileSync('chat.html');
var WebSocket = require('ws');
//var clients = {k: {t: 'sdfs', data: {name: "name_of_sddf"}}};
var clients = {};
var connectionCount = 0;

function printClients() { 
//	console.log('time:', Math.floor(Date.now() / 1000));
	console.log('print clients keys', Object.keys(clients)); };

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
};

getClientAddress = function (req) {
        return (req.headers['x-forwarded-for'] || '').split(',')[0] 
        || req.connection.remoteAddress; };

var server1;
try {
	console.log('trying to create http server');
    server1 = http.createServer(function (req, res) {
            str = req.headers.host;
            ip = str.split(":")[0];
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.end(htmlfile);
        } // function of http server
    ).listen(5100);
} catch (err) {console.log('cant start http server')};

console.log('trying to create wss');
var wss;
var connectionCount = 0;
try {wss = new WebSocket.Server({server: server1}); }
catch (err) {console.log("ERRRRRR WS :", err);};
/*
function getClients() { var a=[]; var i;
	for (i = 0; i<clients.length; i++) {
		b = {i: clients[i].ip, n: clients[i].name};
		a.push(b);
		console.log("B:"+JSON.stringify(b));
	}; return JSON.stringify(a); };
*/

	
wss.on('connection', function(ws, req) {
	ws.id = "w"+connectionCount; connectionCount++;
	ws.ip = req.connection.remoteAddress; // new since WS 3.1
	console.log('ip:', req.connection.remoteAddress);
	console.log('new client id:', ws.id);
	clients[ws.id] = {}; //{data: 'sdjjj'};
printClients();
	clients[ws.id].data = {name: "name_in_wss.on conn", ip: ws.ip, date: new Date()};
	clients[ws.id].ws = ws;	

function sendToClients(msg) { var i;
	console.log("send to clients , number::", Object.keys(clients).length);
	for (var key in clients) { 
		console.log("KEY:", key, " ", JSON.stringify(clients[key].data));
		console.log(msg);
		clients[key].ws.send(msg); 
};
console.log("end send to clients");
};


function isClientExistsOld(name) { var i; var is_exists = false; 
	for (i=0; i<clients.length; i++) { if (clients[i].data.name === name) is_exists= true; };
	return is_exists; };

function isClientExists(name) { var keys = Object.keys(clients); var is_exists = false;
	for (var key in keys) {
		if (key == name) is_exists = true; }; };

function getClientList() { var list = [];
	for (var key in clients) { 
		console.log("client list ", key, 
JSON.stringify(clients[key].data));
		var obj = {}; obj[key] =  clients[key].data;
		console.log('keydata', clients[key].data);
		list.push(clients[key].data); };
	return (list); };

function getClientListHTML() { var list = getClientList(); var rez='';
	for (i=0; i<list.length; i++) {
		rez+="<br>"+list[i].data.name; };
	return rez; };

/*
function getClientList() { var i; var clientList = [];
	for (i=0; i<clients.length; i++) { clientList.push(clients[i].data.name); }; 
	return(clientList); };

function deleteClient(name) { var i; var found = false; var index;
	for (i=0; i<clients.length; i++) { if (clients[i].data.name == name) {
		found = true; index = i; }; };
	if (found) clients.splice(index, 1);
};
*/

function sendUpdatedClientListToAll() { var cl;
	cl = getClientList();
	sendToClients(JSON.stringify({command: 'update_clients', data: cl})); };

function msgProcessor(msg2) { var i; var client;
console.log('msg processor');
printClients();
	var cmd = JSON.parse(msg2);
bd(cmd);
	if (cmd.command == "username_to_server") {
		if (!isClientExists(cmd.data.name)) {
		  ip = ws.ip;
		  clientData = {id: ws.id, ip: ip, name: cmd.data};
		  clients[ws.id] = {};
		  clients[ws.id].data = clientData;
		  clients[ws.id].ws = ws;
		  sendToClients(JSON.stringify({command: 'username_from_server', data: clientData}));
		  sendUpdatedClientListToAll();
		} else {
		  ws.send(JSON.stringify({command: 'username_already_exists', data: ''}));
		};	
	}; // username to server
	if (cmd.command == "message_to_server") { 
		sendToClients(JSON.stringify({command: 'message_from_server', data: cmd.data}));
	}; // message to server
	if (cmd.command == 'private_message') {
		clients[cmd.data.userid].ws.send(JSON.stringify(
			{command: 'private_message_from_server', data: {fromid: ws.id, message: cmd.data.message}}));
	};
	console.log('rcv:-----------------', msg2); 
}; // ws on message (msgProcessor


	console.log('connection 0');
//	while (!ws.readyState) { sleep(100); console.log('waiting'); };
	ws.on('message', msgProcessor);
	ws.on('close', function() {
		//ws.send(JSON.stringify({command: 'user_left', data: clients[ws.id].data.name}));
		console.log('closed for ', clients[ws.id].data.name);  // here was closing bracket for 'on close' and delete clients[ws.id] was called right after creation
		var deletedClientData = clients[ws.id].data;
		delete clients[ws.id];
        sendToClients(JSON.stringify({command: 'user_left', data: deletedClientData}));
		sendUpdatedClientListToAll();
	});

//	ws.send('connection'); 
}); // wss.on ('connection')

console.log('server listening port 80');
