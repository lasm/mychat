/*

TODO:
1. print string before and after bd
2. implement print bd message in several lines depending of screen width
3. print closest function arguments
*/

var DebugTools

DebugTools = function() {
	this.shortFileName = function(fname) { var a1;
		module2pattern = "/([^/]+/[^/]+)$"; //  # 2 last files in path
		a1 = (" "+fname).match(module2pattern);
		return(a1[1]); };
	this.readLine = function(fname, iLine) {
		fs = require('fs');
		mydata = "ss"+fs.readFileSync(fname);
		arr = mydata.split("\n");
		return(arr[iLine]); };

	this.getParamArray = function(callingLineString) {
		var splittedCallingLine = callingLineString.split(/[\s,\(\)]+/);
		var params = [];
//		for (p, i of splittedCallingLine) {
		for (p in splittedCallingLine) {
			if ((splittedCallingLine[p] != '') && (splittedCallingLine[p] != 'bd') &&
				splittedCallingLine[p] != ';'
			) {
				params.push(splittedCallingLine[p]); }; };
		return(params); };

	this.getDebugString = function(args) {
		var divider = ' | '; var prefix = 'bdbd '; var stack = new Error().stack;
		var stackArray = stack.split('\n'); var callingModulePath = stackArray[3];
//console.log('::::::', JSON.stringify(stackArray));
		var fname = callingModulePath.match('[^\\\(]+\\\(([^:]+):')[1];
		var am = '[^\\.]+(^\S+)';
var bb = callingModulePath.split(' ');
//console.log(bb);
var funcname = bb[5].split('.')[1];
if (funcname == '<anonymous>') { funcname = " "; };
		
//		var funcname = callingModulePath.match(am)[1]; 
console.log(funcname);
//console.log('fname::::', fname);
		var callingLineNumberStr = callingModulePath.match(':([^:]+):');
//console.log('call line number str', callingLineNumberStr);
		var callingLineNumberInt = parseInt(callingLineNumberStr[1]) - 1;
		var callingLine = this.readLine(fname, callingLineNumberInt);
		var paramArray = this.getParamArray(callingLine);

		var str1 = prefix+this.shortFileName(fname)+divider + funcname+ ':'+callingLineNumberInt+divider;
		for (var i in paramArray) {
			str1+=paramArray[i] + '=';
			if (typeof(args[i]) == 'object')
				str1+=JSON.stringify(args[i])+'; '
			else
				str1+=args[i] + '; '; };
		return str1; };

	return function() { 
		return console.log(this.getDebugString(arguments)); };
}; // DebugTools

module.exports = DebugTools
